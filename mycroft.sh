#!/bin/sh

set -ex

partition="$1"

{
  printf 'GRUB_TIMEOUT=0\n'
  printf 'GRUB_ENABLE_CRYPTODISK=y\n'
  printf 'GRUB_DISABLE_SUBMENU=y\n'
  printf 'GRUB_DISABLE_RECOVERY=true\n'
  printf 'GRUB_PRELOAD_MODULES="luks cryptodisk part_gpt lvm"\n'
  printf 'GRUB_CMDLINE_LINUX_DEFAULT="modules=sd-mod,usb-storage,btrfs,nvme cryptroot=UUID=%s cryptdm=mycroft cryptkey quiet rootfstype=btrfs"\n' "$(blkid -s UUID -o value "$partition")"
} > /etc/default/grub
dd bs=12 count=4 if=/dev/random of=/crypto_keyfile.bin
chmod 000 /crypto_keyfile.bin
cryptsetup luksAddKey "$partition" /crypto_keyfile.bin
sed -i 's/features="/features="cryptkey /' /etc/mkinitfs/mkinitfs.conf
# shellcheck disable=SC2010
kernel=$(basename "$(ls /lib/modules | grep -v firmware | sort -V | head -n1)")
mkinitfs -c /etc/mkinitfs/mkinitfs.conf "$kernel"

grub-install --target=x86_64-efi --efi-directory=/boot/efi
grub-mkconfig -o /boot/grub/grub.cfg

{
  printf 'UUID=%s swap swap defaults 0 0\n' "$(blkid -s UUID -o value /dev/mycroft/swap)"
  printf 'https://cloud.apiote.xyz/remote.php/webdav/  /media/nextcloud/me@cloud.apiote.xyz/  davfs rw,user,uid=1000,noauto 0 0'
} >>/etc/fstab
rc-update add swap boot

cd /
git init
git remote add origin https://git.apiote.xyz/git/mycroft.git
git fetch
git checkout origin/master -ft
apk update
apk fix
