#!/bin/sh

set -ex

cd /
git init
git remote add origin https://git.apiote.xyz/git/deepthought.git
git fetch
git checkout origin/master -ft
apk update
apk fix
openrc default
